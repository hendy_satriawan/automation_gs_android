*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DateTime

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Variables ***
${emaillogin}   hendy.satriawan@gmail.com
${passlogin}    1234567


*** Keywords ***
Buka semua Tab Halaman Register
  Wait Until Page Contains Element    //android.widget.ImageView[contains(@resource-id,'com.guesehat.android:id/toolbar_logo')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Daftar Baru')][@selected='true']    ${timeout}
  # masuk ke tab masuk / login
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Masuk')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Masuk')]
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Masuk')][@selected='true']    ${timeout}
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_login')][@text='LOGIN']    ${timeout}
  # masuke ke tab lupa sandi
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Lupa Sandi')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Lupa Sandi')]
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Lupa Sandi')][@selected='true']   ${timeout}
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_send')][@text='SEND']    ${timeout}

Skip Ke Home
  #pilih skip untuk masuk ke home
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/action_skip')][@text='SKIP']   ${timeout}
  ${start}    Get Current Date
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/action_skip')][@text='SKIP']
  # cek masuk ke halaman home
  # Wait Until Page Does Not Contain Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/label')][@text='Please wait']    20s
  Wait Until Element Is Visible    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_home')]   7s
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_category')]   10s
  ${end}    Get Current Date
  ${diff}   Subtract Date From Date    ${end}    ${start}   verbose
  Set Tags    [ TIME ] Loading Muncul Home via Tombol Skip: ${diff}

Login Google
  Wait Until Page Contains Element    //android.widget.ImageView[contains(@resource-id,'com.guesehat.android:id/toolbar_logo')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Daftar Baru')][@selected='true']    ${timeout}
  # masuk ke tab masuk / login
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Masuk')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Masuk')]
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Masuk')][@selected='true']
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_login')][@text='LOGIN']    ${timeout}
  # pilih google
  Wait Until Page Contains Element    //android.widget.ImageView[contains(@resource-id,'com.guesehat.android:id/btn_google')]   ${timeout}
  Click Element    //android.widget.ImageView[contains(@resource-id,'com.guesehat.android:id/btn_google')]
  Permission_Gallery
  Wait Until Element Is Visible    //android.widget.ImageView[contains(@resource-id,'com.guesehat.android:id/btn_google')]   ${timeout}
  Click Element    //android.widget.ImageView[contains(@resource-id,'com.guesehat.android:id/btn_google')]
  #masuk halaman google
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.google.android.gms:id/main_title')][@text='Pilih akun']    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.google.android.gms:id/account_name')][@text='sampah.hendy5@gmail.com']   ${timeout}
  ${start}    Get Current Date
  Click Element    //android.widget.TextView[contains(@resource-id,'com.google.android.gms:id/account_name')][@text='sampah.hendy5@gmail.com']
  #masuk ke halaman profile
  ${profile}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')]
  Run Keyword If    ${profile}    Run Keywords    Click Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_save')]
  ...    AND    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_profession')]
  ...    AND    Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_profession')]
  ...    AND    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_title')][@text='Seniman']
  ...    AND    Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_title')][@text='Seniman']
  ...    AND    Wait Until Page Contains Element    //android.widget.CheckBox[contains(@resource-id,'com.guesehat.android:id/cb_picker')][@checked='true']
  ...    AND    Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/done')]
  ...    AND    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_birth_date')]
  ...    AND    Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_birth_date')]
  ...    AND    Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'android:id/button1')]
  ...    AND    Click Element    //android.widget.Button[contains(@resource-id,'android:id/button1')]
  ...    AND    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_city')]
  ...    AND    Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_city')]
  ...    AND    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_title')][@text='Aceh  Tenggara']
  ...    AND    Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_title')][@text='Aceh  Tenggara']
  ...    AND    Wait Until Page Contains Element    //android.widget.CheckBox[contains(@resource-id,'com.guesehat.android:id/cb_picker')][@checked='true']
  ...    AND    Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/done')]
  ...    AND    Input Text    //android.widget.EditText[contains(@resource-id,'com.guesehat.android:id/et_description')]    isi aja
  ...    AND    Hide Keyboard
  ...    AND    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_gender')]
  ...    AND    Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_gender')]
  ...    AND    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'android:id/text1')][@text='Pria']
  ...    AND    Click Element    //android.widget.TextView[contains(@resource-id,'android:id/text1')][@text='Pria']
  ...    AND    Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_save')]
  ...    AND    Click Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_save')]
  ...    AND    Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]     ${timeout}
  ...    AND    Click Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]
  # berhasil login & masuk ke halaman HOME
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_home')]    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_category')]   ${timeout}
  ${end}    Get Current Date
  ${diff}   Subtract Date From Date    ${end}    ${start}   verbose
  Set Tags    [ TIME ] Loading Muncul Home via Login Google : ${diff}
  # cek sudah berhasil login
  Click Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]
  Wait Until Page Does Not Contain Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_logged_in')][@text='Login']   ${timeout}

Login Facebook
  Wait Until Page Contains Element    //android.widget.ImageView[contains(@resource-id,'com.guesehat.android:id/toolbar_logo')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Daftar Baru')][@selected='true']    ${timeout}
  # masuk ke tab masuk / login
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Masuk')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Masuk')]
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Masuk')][@selected='true']
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_login')][@text='LOGIN']    ${timeout}
  # pilih facebook
  Wait Until Page Contains Element    //android.widget.ImageView[contains(@resource-id,'com.guesehat.android:id/btn_facebook')]   ${timeout}
  Click Element    //android.widget.ImageView[contains(@resource-id,'com.guesehat.android:id/btn_facebook')]
  Permission_Storage
  Wait Until Page Contains Element    //android.widget.ImageView[contains(@resource-id,'com.guesehat.android:id/btn_facebook')]   ${timeout}
  Click Element    //android.widget.ImageView[contains(@resource-id,'com.guesehat.android:id/btn_facebook')]
  #masuk ke halaman facebook
  Wait Until Page Contains Element    //android.view.View[contains(@text,'facebook')]   ${timeout}
  Wait Until Page Contains Element    //android.view.View[contains(@text,'Masuk ke akun Facebook Anda untuk terhubung dengan GueSehat')]    ${timeout}
  #input email
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'m_login_email')]   ${timeout}
  Input Text    //android.widget.EditText[contains(@resource-id,'m_login_email')]    ${email_fb}
  Hide Keyboard
  # input password
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'m_login_password')]    ${timeout}
  Input Text    //android.widget.EditText[contains(@resource-id,'m_login_password')]    ${pass_fb}
  Hide Keyboard
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'u_0_5')]   ${timeout}
  Click Element    //android.widget.Button[contains(@resource-id,'u_0_5')]
  #confirm login
  Sleep    3s
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'u_0_1')][@text='Continue']   ${timeout}
  ${start}    Get Current Date
  Click Element    //android.widget.Button[contains(@resource-id,'u_0_1')][@text='Continue']
  # berhasil login & masuk ke halaman HOME
  # Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/label')][@text='Please wait']    ${timeout}
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_home')]    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_category')]   ${timeout}
  ${end}    Get Current Date
  ${diff}   Subtract Date From Date    ${end}    ${start}   verbose
  Set Tags    [ TIME ] Loading Muncul Home via Login Facebook : ${diff}
  # cek sudah berhasil login
  Click Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]
  Wait Until Page Does Not Contain Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_logged_in')][@text='Login']   ${timeout}

Login Via Side Menu
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Click Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]
  #pilih login
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_logged_in')][@text='Login']   ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_logged_in')][@text='Login']
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Masuk')]    ${timeout}
  #masuk ke tab masuk
  Click Element    //android.widget.TextView[contains(@text,'Masuk')]
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Masuk')][@selected='true']    ${timeout}
  Hide Keyboard
  Input Text    //android.widget.EditText[contains(@resource-id,'com.guesehat.android:id/et_input')][@text='Email']    ${emaillogin}
  Hide Keyboard
  Input Password    //android.widget.EditText[contains(@resource-id,'com.guesehat.android:id/et_input')][@text='Password']    ${passlogin}
  Hide Keyboard
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_login')]   ${timeout}
  Click Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_login')]
  # login sukses, masuk ke halaman home
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/action_search')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_home')]    ${timeout}
