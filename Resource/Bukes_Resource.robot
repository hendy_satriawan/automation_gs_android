*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Variables ***
${back_keycode}   4

*** Keywords ***
Buka Bukes
  #masuk ke halaman bukes via bottom menu
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_bukes')]   ${timeout}
  Click Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_bukes')]
  # cek halaman bukes
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Buku Kesehatan']    ${timeout}
  Wait Until Page Contains Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_imt')]
  Wait Until Page Contains Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_siklus_haid')]
  Wait Until Page Contains Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_pregnancy')]
  Wait Until Page Contains Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_tumbuh_kembang')]

Masuk Hitung Kalori IMT
  Wait Until Page Contains Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_imt')]    ${timeout}
  Click Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_imt')]
  # cek halaman IMT
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Indeks Massa Tubuh']    ${timeout}
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_start')][@text='MULAI']    ${timeout}

Kembali ke Halaman Bukes
  # klik back device android -> saat ini kalau klik back malah kembali keluar dari menu bukes
  Press Keycode    ${back_keycode}

Masuk Siklus Haid
  Wait Until Page Contains Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_siklus_haid')]   ${timeout}
  Click Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_siklus_haid')]
  # masuk ke halaman siklus haid
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Siklus Haid']   ${timeout}
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_start')][@text='MULAI']    ${timeout}

Masuk Kehamilan
  Wait Until Page Contains Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_pregnancy')]   ${timeout}
  Click Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_pregnancy')]
  # masuk ke halaman kehamilan
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Kehamilan']   ${timeout}
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_start')][@text='MULAI']    ${timeout}

Masuk Tumbuh Kembang
  Wait Until Page Contains Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_tumbuh_kembang')]   ${timeout}
  Click Element    //android.widget.FrameLayout[contains(@resource-id,'com.guesehat.android:id/cv_tumbuh_kembang')]
  # masuk ke halaman tumbuh kembang
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Tumbuh Kembang Anak']   ${timeout}
  Wait Until Page Contains Element    //android.widget.LinearLayout[contains(@resource-id,'com.guesehat.android:id/tv_register_child')]    ${timeout}
