*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***
Notifikasi Side Menu
  Wait Until Page Contains Element    //android.widget.ImageButton[@content-desc="Navigasi naik"]   ${timeout}
  Click Element    //android.widget.ImageButton[@content-desc="Navigasi naik"]
  # buka drawer menu notifikasi
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/notification_label')][@text='Notification']     ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/notification_label')][@text='Notification']
  # masuk ke halaman notifikasi
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Notifications']    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/action_search')]     ${timeout}
  # masuk ke halaman cari
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/action_search')]     ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/action_search')]
  # cek halaman cari
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'com.guesehat.android:id/et_search')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_cancel')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_cancel')]
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Notifications']    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/action_search')]     ${timeout}
