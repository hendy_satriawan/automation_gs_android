*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***
Sakit Apa
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_sakit_apa')]   ${timeout}
  Click Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_sakit_apa')]
  # popup info
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/txtTitle')][@text='Perhatian']    ${timeout}
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_OK')]    ${timeout}
  Click Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_OK')]
  # cek halaman sakit apa
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Sakit Apa']   ${timeout}
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_start')][@text='MULAI']    ${timeout}
