*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot


*** Variables ***
${cari_Penyakit}    Bronkitis


*** Keywords ***
Side Menu Penyakit
  Wait Until Page Contains Element    //android.widget.ImageButton[@content-desc="Navigasi naik"]   ${timeout}
  Click Element    //android.widget.ImageButton[@content-desc="Navigasi naik"]
  # buka drawer menu
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/label_Disease')][@text='Penyakit']     ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/label_Disease')][@text='Penyakit']
  # masuk ke halaman Penyakit
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Penyakit']    ${timeout}
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'com.guesehat.android:id/et_search_disease')]     ${timeout}
  # buka penyakit abses gigi
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_title')][@text='Abses Gigi']    ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_title')][@text='Abses Gigi']
  # masuk ke penyakit abses gigi & tab deskripsi
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_title')][@text='Abses Gigi']    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Deskripsi')][@selected='true']      ${timeout}
  # masuk ke tab pencegahan
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Pencegahan')][@selected='false']    ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Pencegahan')][@selected='false']
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Pencegahan')][@selected='true']     ${timeout}
  # masuk ke tab gejala
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Gejala')][@selected='false']    ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Gejala')][@selected='false']
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Gejala')][@selected='true']     ${timeout}
  # masuk ke tab penyebab
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Penyebab')][@selected='false']    ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Penyebab')][@selected='false']
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Penyebab')][@selected='true']     ${timeout}
  # masuk ke tab diagnosa
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Diagnosis')][@selected='false']    ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Diagnosis')][@selected='false']
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Diagnosis')][@selected='true']     ${timeout}
  # masuk ke tab penanganan
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Penanganan')][@selected='false']    ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Penanganan')][@selected='false']
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Penanganan')][@selected='true']     ${timeout}
  # share penyakit
  Wait Until Page Contains Element    //android.widget.TextView[@content-desc="Share"]    ${timeout}
  Click Element    //android.widget.TextView[@content-desc="Share"]
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'android:id/title_default')][@text='Bagikan mengenai penyakit']   ${timeout}
  Press Keycode    ${back_keycode}

Kembali Ke Halaman Penyakit Dari Detail Penyakit
  Wait Until Page Contains Element    //android.widget.ImageButton[@content-desc="Navigasi naik"]   ${timeout}
  Click Element    //android.widget.ImageButton[@content-desc="Navigasi naik"]
  # cek halaman Penyakit
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Penyakit']    ${timeout}
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'com.guesehat.android:id/et_search_disease')]     ${timeout}

Cari Penyakit
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'com.guesehat.android:id/et_search_disease')]   ${timeout}
  Click Element    //android.widget.EditText[contains(@resource-id,'com.guesehat.android:id/et_search_disease')]
  Input Text    //android.widget.EditText[contains(@resource-id,'com.guesehat.android:id/et_search_disease')]    ${cari_Penyakit}
  klik cari
  # cek hasil
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_title')][@text='${cari_Penyakit}']    ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_title')][@text='${cari_Penyakit}']
