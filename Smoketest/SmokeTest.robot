*** Setting ***

Library    AppiumLibrary
Library    BuiltIn

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Register_Resource.robot
Resource    ../Resource/Home_Resource.robot
Resource    ../Resource/Direktori_Resource.robot
Resource    ../Resource/Artikel_Resource.robot
Resource    ../Resource/Event_Resource.robot
Resource    ../Resource/Profile_Resource.robot
Resource    ../Resource/Bukes_Resource.robot
Resource    ../Resource/SakitApa_Resource.robot
Resource    ../Resource/TanyaDokter_Resource.robot
Resource    ../Resource/Penyakit_Resource.robot
Resource    ../Resource/Notifikasi_Resource.robot
Resource    ../Resource/Setting_Resource.robot

*** Test Cases ***
1.Ke halaman GS (Onboarding)
  [Tags]   swipe splashscreen sampai masuk ke halaman beranda
  Buka Apps GS Real Device
  Force Update
  Splash Screen GS
  Buka semua Tab Halaman Register
  Skip Ke Home
  Close Application

2.Register akun Guesehat
  [Tags]   Register akun sampai aktivasi akun via chrome
  Buka Apps GS Real Device
  Force Update
  Splash Screen GS
  Register
  Close Application

3.Forgot Password akun Guesehat
  [Tags]   Forgot Password dengan akun yang baru di register
  Buka Apps GS Real Device
  Force Update
  Splash Screen GS
  Forgot Password
  Close Application

4.login dengan akun google
  [Tags]   Login aplikasi GS dengan akun email google yang sudah terdaftar
  Buka Apps GS Real Device
  Force Update
  Splash Screen GS
  Login Google
  Close Application

5.login dengan akun facebook
  [Tags]   Login aplikasi GS dengan akun facebook yang sudah terdaftar
  Buka Apps GS Real Device
  Force Update
  Splash Screen GS
  Login Facebook
  Close Application

6.Home
  [Tags]   view & open fitur dihalaman home
  Buka Apps GS Real Device
  Force Update
  Splash Screen GS
  Skip Ke Home
  Home Slide Artikel
  Kembali ke Home dari slide artikel

7.Home Slide Direktori Dokter
  [Tags]   open direktori dokter via slide direktori di home
  Dokter Slide Direktori
  Kembali ke Home via side menu

8.Home Slide Direktori Rumah Sakit
  [Tags]   open Rumah sakit via slide direktori di home
  Rumah sakit slide Direktori
  Kembali ke Home via Bottom Bar

9.Home Slide Direktori Klinik
  [Tags]   open direktori klinik via slide direktori di home
  Klinik slide Direktori
  Kembali ke Home via button back device

10.Home Slide Direktori Gym & Health Club
  [Tags]   open direktori Gym & Health Club via slide direktori di home
  Gym & Health Club slide Direktori
  Kembali ke Home via button back device

11.Home Slide Direktori Spa & Massage
  [Tags]   open direktori Spa & Massage via slide direktori di home
  Spa & Massage slide Direktori
  Kembali ke Home via button back device

12.Home artikel kategori Lifestyle
  [Tags]   open artikel kategori Lifestyle di Home
  Home Artikel kategori Lifestyle
  Kembali ke Home via button back device

13.Home artikel kategori Medis
  [Tags]   open artikel kategori Medis di Home
  Home Artikel kategori Medis
  Kembali ke Home via button back device

14.Home artikel kategori Sex & Love
  [Tags]   open artikel kategori Sex & Love di Home
  Home Artikel kategori Sex & Love
  Kembali ke Home via button back device

15.Home artikel kategori Wanita
  [Tags]   open artikel kategori Wanita di Home
  Home Artikel kategori Wanita
  Kembali ke Home via button back device

16.Home Search
  [Tags]   cari di home dengan input keyword tertentu & open artikel/forum/dokter
  Search Home Artikel
  Kembali ke search dari detail
  Search Home Forum
  Kembali ke search dari detail
  Search Home Dokter
  Kembali ke search dari detail
  Kembali ke Home dari Search
  Close Application

17.Direktori Dokter
  [Tags]   open direktori dokter via bottom menu & open detail dokter
  Buka Apps GS Real Device
  Force Update
  Splash Screen GS
  Skip Ke Home
  Direktori Dokter & Detail
  Kembali ke Direktori dokter dari detail
  Direktori Dokter Filter & Hapus Filter

18.Direktori Rumah Sakit
  [Tags]   open direktori rumah sakit via bottom menu & open detail rumah sakit
  Direktori Rumah Sakit & Detail
  Kembali ke Direktori Rumah Sakit dari Detail

19.Direktori Klinik
  [Tags]   open direktori klinik via bottom menu & open detail klinik
  Direktori Klinik & Detail
  Kembali ke Direktori Klinik dari Detail

20.Direktori Gym & Health Club
  [Tags]   open direktori Gym & Health Club via bottom menu & open detail Gym & Health Club
  Direktori Gym Health Club & Detail
  Kembali ke Direktori Gym & Health Club dari Detail

21.Direktori Spa & Massage
  [Tags]   open direktori Spa & Massage via bottom menu & open detail Spa & Massage
  Direktori Spa Massage & Detail
  Kembali ke Direktori Spa & Massage dari Detail

22.Direktori Healthy Food & Beverage
  [Tags]   open direktori Healthy Food & Beverage via bottom menu & open detail Healthy Food & Beverage
  Direktori Healthy Food Beverage & Detail
  Kembali ke Direktori Healthy Food & Beverage dari Detail

23.Direktori Beauty
  [Tags]   open direktori Beauty via bottom menu & open detail Beauty
  Direktori Beauty & Detail
  Kembali ke Direktori Beauty dari Detil

24.Direktori Lab
  [Tags]   open direktori Lab via bottom menu & open detail Lab
  Direktori Lab & Detail
  Kembali ke Direktori Lab dari Detil

25.Direktori Praktisi
  [Tags]   open direktori Praktisi via bottom menu & open detail Praktisi
  Direktori Praktisi & Detail
  Kembali ke Direktori Praktisi dari Detil
  Close Application

26.Bukes Hitung Kalori
  [Tags]   open bukes dari bottom menu lalu masuk ke menu hitung kalori & IMT
  Buka Apps GS Real Device
  Force Update
  Splash Screen GS
  Skip Ke Home
  Buka Bukes
  Masuk Hitung Kalori IMT
  Kembali ke Halaman Bukes

27.Bukes Siklus Haid
  [Tags]   open bukes dari bottom menu lalu masuk ke menu siklus haid
  Buka Bukes
  Masuk Siklus Haid
  Kembali ke Halaman Bukes

28.Bukes Kehamilan
  [Tags]   open bukes dari bottom menu lalu masuk ke menu kehamilan
  Buka Bukes
  Masuk Kehamilan
  Kembali ke Halaman Bukes

29.Bukes Tumbuh Kembang
  [Tags]   open bukes dari bottom menu lalu masuk ke menu Tumbuh Kembang
  Buka Bukes
  Masuk Tumbuh Kembang
  Kembali ke Halaman Bukes

30.Sakit Apa
  [Tags]   open sakit apa dari bottom menu
  Buka Apps GS Real Device
  Force Update
  Splash Screen GS
  Skip Ke Home
  Sakit Apa
  Close Application

# # # 10.Tanya Dokter == sudah di drop
# # #   [Tags]   open Tanya Dokter dari bottom menu
# # #   Buka Apps GS Real Device
# # #   Force Update
# # #   Splash Screen GS
# # #   Skip Ke Home
# # #   Tanya Dokter
# # #   Kembali ke Home via side menu
# # #   Close Application

31.Artikel Tab Semua
  [Tags]   open artikel tab semua via side menu & open artikel tab semua
  Buka Apps GS Real Device
  Force Update
  Splash Screen GS
  Skip Ke Home
  Masuk Artikel via Side Menu & Detail Semua artikel

32.Artikel Tab Lifestyle
  [Tags]   open artikel tab Lifestyle via side menu & open artikel tab Lifestyle
  Masuk Artikel via Side Menu & Detail Lifestyle Artikel
  Kembali ke Artikel Lifestyle dari Detil Artikel

33.Artikel Tab Medis
  [Tags]   open artikel tab Medis via side menu & open artikel tab Medis
  Masuk Artikel via Side Menu & Detail Medis Artikel
  Kembali ke Artikel Medis dari Detil Artikel

34.Artikel Tab Sex & Love
  [Tags]   open artikel tab Sex & Love via side menu & open artikel tab Sex & Love
  Masuk Artikel via Side Menu & Detail Sex Love Artikel
  Kembali ke Artikel Sex Love dari Detil Artikel

35.Artikel Tab Wanita
  [Tags]   open artikel tab Wanita via side menu & open artikel tab Wanita
  Masuk Artikel via Side Menu & Detail Wanita Artikel
  Kembali ke Artikel Wanita dari Detil Artikel

36.Cari artikel
  [Tags]   cari artikel & masuk ke detail artikel
  Cari Artikel & Masuk ke Detail
  Kembali ke Artikel dari Pencarian Artikel
  Ke Home

37.Artikel Pagination
  [Tags]   open artikel Pagination via side menu & open artikel Pagination
  Artikel Pagination & Detail
  Kembali ke Home dari Artikel Pagination

38.Artikel Slideshow
  [Tags]   open artikel Slideshow via side menu & open artikel Slideshow
  Artikel Slideshow & Detail
  Kembali ke Home dari Artikel Slideshow

39.Artikel Counting
  [Tags]   open artikel Counting via side menu & open artikel Counting
  Login Via Side Menu
  Artikel Counting & Detail
  Kembali ke Home dari Artikel Counting

40.Artikel Trivia
  [Tags]   open artikel Trivia via side menu & open artikel Trivia
  Artikel Trivia & Detail
  Kembali ke Home dari Artikel Trivia

41.Artikel Survey
  [Tags]   open artikel Survey via side menu & open artikel Survey
  Artikel Survey & Detail
  Kembali ke Home dari Artikel Survey
  Close Application

42.Event
  [Tags]   cari event & masuk ke detail event
  Buka Apps GS Real Device
  Force Update
  Splash Screen GS
  Skip Ke Home
  Masuk Halaman Event
  Kembali ke Event dari Detil Event
  Share Event
  Ke Home

43.Profile & Edit Profile
  [Tags]   open Profile & edit profile
  Login Via Side Menu
  Masuk & Edit Profile
  Kembali ke Home dari Profile

44.Penyakit
  [Tags]   Buka halaman penyakit dari side menu
  Side Menu Penyakit
  Kembali Ke Halaman Penyakit Dari Detail Penyakit
  Cari Penyakit
  Kembali Ke Halaman Penyakit Dari Detail Penyakit

# # # 15.Tanya Dokter == di drop
# # #   [Tags]   Buka halaman tanya dokter dari side menu
# # #   Tanya Dokter Side Menu

45.Notification
  [Tags]   Buka halaman Notification dari side menu
  Notifikasi Side Menu

46.Setting
  [Tags]   Buka halaman Settings dari side menu
  Setting Side Menu
