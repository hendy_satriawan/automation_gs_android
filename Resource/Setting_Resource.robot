*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***
Setting Side Menu
  Wait Until Page Contains Element    //android.widget.ImageButton[@content-desc="Navigasi naik"]   ${timeout}
  Click Element    //android.widget.ImageButton[@content-desc="Navigasi naik"]
  # buka drawer menu setting
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/setting_label')][@text='Setting']   ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/setting_label')][@text='Setting']
  # cek halaman setting
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Setting']
