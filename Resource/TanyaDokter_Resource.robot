*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***
Tanya Dokter
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_consultation')]    ${timeout}
  Click Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_consultation')]
  # masuk ke halaman tanya dokter
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Tanya Dokter']    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tvNameDoctor')]     ${timeout}

Tanya Dokter Side Menu
  Wait Until Page Contains Element    //android.widget.ImageButton[@content-desc="Navigasi naik"]   ${timeout}
  Click Element    //android.widget.ImageButton[@content-desc="Navigasi naik"]
  # buka drawer menu tanya dokter
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/label_chat')][@text='Tanya Dokter']     ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/label_chat')][@text='Tanya Dokter']
  # masuk ke halaman tanya dokter
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Tanya Dokter']    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tvNameDoctor')]     ${timeout}
